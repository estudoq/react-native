import React from 'react';
// import {Text} from 'react-native';

import PrimeiroComponente from '../components/Primeiro';

// function App() {
//   const jsx = <Text> Primeiro componente </Text>;

//   return jsx;
// }

// const App = function App() {
//   const jsx = <Text> Primeiro componente! </Text>;

//   return jsx;
// };

// export default App;

export default () => {
  return <PrimeiroComponente></PrimeiroComponente>;
};
