import React from 'react';
import {View, Image, StyleSheet} from 'react-native';

//Layout da imagem
const styles = StyleSheet.create({
  tinyLogo: {
    width: 50,
    height: 50,
  },
});

//Definição do objeto
const DisplayImage = () => {
  return (
    <View>
      <Image
        style={styles.tinyLogo}
        source={{
          uri: 'https://reactnative.dev/img/tiny_logo.png',
        }}
      />
    </View>
  );
};

export default DisplayImage;
