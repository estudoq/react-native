import React, {FC} from 'react';
import {
  Dimensions,
  KeyboardTypeOptions,
  StyleSheet,
  TextInput,
  View,
} from 'react-native';

// const {height, width} = Dimensions.get('screen') - retorna as dimensões da tela
const {width} = Dimensions.get('screen');

//Layout do campo de texto
const styles = StyleSheet.create({
  container: {
    alignSelf: 'center',
    width: width / 1.1,
    marginVertical: 10,
  },
  textInput: {
    backgroundColor: '#4444442b',
    borderRadius: 5,
    fontSize: 15,
  },
});

//Definição da interface a ser implementada
interface Props {
  placeHolder: string;
  onChangeText: (text: string) => void;
  secureTextEntry?: boolean;
  keyBoardType?: KeyboardTypeOptions;
}

//Definição do objeto
const Input: FC<Props> = props => {
  return (
    <View style={styles.container}>
      <TextInput
        style={styles.textInput}
        placeholder={props.placeHolder}
        secureTextEntry={props.secureTextEntry}
        onChangeText={props.onChangeText}
        keyboardType={props.keyBoardType}
      />
    </View>
  );
};

export default Input;
