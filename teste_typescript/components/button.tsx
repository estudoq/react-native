import React, {FC} from 'react';
import {Dimensions, TouchableOpacity, Text} from 'react-native';
import {StyleSheet} from 'react-native';

const {width} = Dimensions.get('screen');

//Layout do botão
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#000',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    padding: 10,
    borderRadius: 8,
    width: width / 3,
  },
  text: {
    color: '#fff',
    fontSize: 15,
  },
});

//Definição da interface a ser implementada
interface Props {
  title: string;
  onPress: () => void;
}

//Definição do objeto
const MyButton: FC<Props> = props => {
  return (
    <TouchableOpacity style={styles.container} onPress={props.onPress}>
      <Text style={styles.text}> {props.title} </Text>
    </TouchableOpacity>
  );
};

export default MyButton;
