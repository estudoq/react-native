import React, {useState, FC} from 'react';
import {Text, SafeAreaView, StatusBar, StyleSheet, Alert} from 'react-native';
import Input from '../components/input';
import Button from '../components/button';
import Image from '../components/image';

//Definindo layouts dos componentes
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    fontSize: 16,
    fontWeight: 'bold',
    margin: 16,
  },
});

const App: FC = () => {
  const [email, setEmail] = useState<string | null>(null);
  const [passwd, setPasswd] = useState<string | null>(null);

  return (
    /* SafeAreaView - o componente garante que os elementos serão exibidos apenas
       onde será possível vê-los na tela
    */
    <SafeAreaView style={styles.container}>
      {/* Definindo cor da barra de status */}
      <StatusBar backgroundColor="#000" animated={true} />

      {/* Utilizando o componente criado 'Image' */}
      <Image />

      <Text style={styles.text}> Login </Text>

      {/* Utilizando o componente criado 'Input' */}
      <Input
        placeHolder="Email"
        onChangeText={text => setEmail(text)}
        keyBoardType={'email-address'}
      />
      <Input
        placeHolder="Senha"
        onChangeText={text => setPasswd(text)}
        secureTextEntry={true}
      />

      {/* Utilizando o componente criado 'Button' */}
      <Button
        title="Entrar"
        onPress={() => {
          if (email && passwd) {
            //Estrutura de um dialog -> alert(título, mensagem, botões, opções)
            Alert.alert(
              'Informações',
              'Usuário: ' + email + '\nSenha: ' + passwd,
              [
                {
                  text: 'OK',
                  onPress: () => console.log('OK pressed'),
                },
              ],
              {
                cancelable: true,
                onDismiss: () => console.log('Alert dismissed'),
              },
            );
          } else {
            Alert.alert('Atenção', 'Preencha todos os campos!');
          }
        }}
      />
    </SafeAreaView>
  );
};

export default App;
