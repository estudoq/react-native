import { addDays } from 'date-fns';

export function getDate(date: Date){
    return addDays(date, 1);
}