import { StackNavigationProp } from "@react-navigation/stack";
import { CarDTO } from "../dtos/CarsDTO";
import { UserDTO } from "../dtos/UserDTO";

type RootStackParamList = {
    Splash: undefined;
    SignIn: undefined;
    SignUpFirstStep: undefined;
    SignUpSecondStep: { user: UserDTO };
    Home: undefined;
    CarDetails: { car: CarDTO };
    Scheduling: { car: CarDTO };
    SchedulingDetails: { car: CarDTO, dates: string[] };
    Confirmation: { 
        title: string,
        message: string,
        nextScreenRoute: any 
    };
    MyCars: undefined;
}

export type AppNavigationProps = StackNavigationProp<RootStackParamList>;