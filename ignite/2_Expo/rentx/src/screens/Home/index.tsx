import React, { useCallback, useState } from 'react';
import { useFocusEffect, useNavigation } from '@react-navigation/native';
import { RectButton, PanGestureHandler } from 'react-native-gesture-handler';
import { Alert, StatusBar, StyleSheet, BackHandler } from 'react-native';
import { RFValue } from 'react-native-responsive-fontsize';
import { Ionicons } from '@expo/vector-icons';
import { useTheme } from 'styled-components';

import Animated, {
    useAnimatedStyle,
    useAnimatedGestureHandler,
    useSharedValue,
    withSpring
} from 'react-native-reanimated';

import Logo from '../../assets/logo.svg';
import { Car } from '../../components/Car';
import { AppNavigationProps } from '../../utils/stackNavigation';
import { LoadAnimation } from '../../components/LoadAnimation';

import api from '../../services/api';
import { CarDTO } from '../../dtos/CarsDTO';

import {
    Container,
    Header,
    HeaderContent,
    TotalCars,
    CarList,
    GestureHandlerView
} from './styles';

const ButtonAnimated = Animated.createAnimatedComponent(RectButton)

export function Home(){
    const [cars, setCars] = useState<CarDTO[]>([]);
    const [loading, setLoading] = useState({} as boolean);

    const positionY = useSharedValue(0);
    const positionX = useSharedValue(0);

    const myCarsButtonStyle = useAnimatedStyle(() => {
        return {
            transform: [
                { translateX: positionX.value },
                { translateY: positionY.value }
            ]
        }
    });

    const onGestureEvent = useAnimatedGestureHandler({
        onStart(_, ctx: any){
            ctx.positionX = positionX.value;
            ctx.positionY = positionY.value;
        },
        onActive(event, ctx){
            positionX.value = ctx.positionX + event.translationX;
            positionY.value = ctx.positionY + event.translationY;
        },
        onEnd(){
            positionX.value = withSpring(0);
            positionY.value = withSpring(0);
        }
    });

    const navigation = useNavigation<AppNavigationProps>();
    const theme = useTheme();

    function handleCarDetails(car: CarDTO){
        navigation.navigate("CarDetails", { car });
    }

    function handleOpenMyCars(){
        navigation.navigate("MyCars");
    }

    useFocusEffect(useCallback(() => {
        async function fetchCars(){
            try {
                setLoading(true);
                const response = await api.get("/cars");
                setCars(response.data);
            } catch (error) {
                console.log(error as string);
                Alert.alert("ERRO", "Não foi possível carregar a lista de carros!");
            } finally {
                setLoading(false);
            }
        }

        fetchCars();
    }, []));

    useFocusEffect(() => {
        const backHandler = BackHandler.addEventListener('hardwareBackPress', () => { return true });
        return () => backHandler.remove();
    });

    return (
        <Container>
            <StatusBar 
                barStyle="light-content"
                backgroundColor="transparent"
                translucent
            />
            <Header>
                <HeaderContent>
                    <Logo 
                        width={RFValue(108)}
                        height={RFValue(12)}
                    />
                    {
                        !loading &&
                        <TotalCars>
                            Total de {cars.length} carros
                        </TotalCars>
                    }
                </HeaderContent>
            </Header>

            { loading ? <LoadAnimation /> : 
                <CarList 
                    data={cars}
                    keyExtractor={item => item.id}
                    renderItem={({ item }) => 
                        <Car data={item} onPress={() => handleCarDetails(item)}/> 
                    }
                />
            }
            
            <PanGestureHandler onGestureEvent={onGestureEvent}>
                <Animated.View
                    style={[
                        myCarsButtonStyle,
                        {
                            position: 'absolute',
                            bottom: 13,
                            right: 22
                        }
                    ]}
                >
                    <GestureHandlerView>
                        <ButtonAnimated 
                            onPress={handleOpenMyCars}
                            style={[styles.button, { backgroundColor: theme.colors.main }]}
                        >
                            <Ionicons 
                                name="ios-car-sport" 
                                size={RFValue(32)}
                                color={theme.colors.shape}
                            />
                        </ButtonAnimated>
                    </GestureHandlerView>
                </Animated.View>
            </PanGestureHandler>

        </Container>
    );
}

const styles = StyleSheet.create({
    button: {
        width: RFValue(60),
        height: RFValue(60),
        borderRadius: RFValue(30),
        justifyContent: 'center',
        alignItems: 'center'
    }
})
