import React, { useState } from 'react';
import { useNavigation, useRoute } from '@react-navigation/native';

import { BackButton } from '../../../components/BackButton';
import { Bullet } from '../../../components/Bullet';
import { AppNavigationProps } from '../../../utils/stackNavigation';

import {
    Container,
    Header,
    Steps,
    Title,
    SubTitle,
    Form,
    FormTitle
} from './styles';

import { PasswordInput } from '../../../components/PasswordInput';
import { Alert, Keyboard, KeyboardAvoidingView, TouchableWithoutFeedback } from 'react-native';
import { Button } from '../../../components/Button';
import { useTheme } from 'styled-components';
import { UserDTO } from '../../../dtos/UserDTO';

interface Params {
    user: UserDTO;
}

export function SignUpSecondStep(){
    const [password, setPassword] = useState('');
    const [passwordConfirm, setPasswordConfirm] = useState('');
    
    const navigation = useNavigation<AppNavigationProps>();
    const route = useRoute();
    const theme = useTheme();

    const { user } = route.params as Params;

    function handleRegister(){
        if(!password || !passwordConfirm) {
            return Alert.alert('ERRO', 'Informe a senha e a confirmação');
        }

        if(password != passwordConfirm) {
            return Alert.alert('ERRO', 'As senhas não são iguais');
        }

        navigation.navigate("Confirmation", {
            title: "Conta criada!",
            message: `Agora é só fazer login\ne aproveitar`,
            nextScreenRoute: "SignIn"
        })
    }

    function handleBack(){
        navigation.goBack();
    }

    return (
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <Container>
                <KeyboardAvoidingView enabled>
                    <Header>
                        <BackButton onPress={handleBack} />
                        <Steps>
                            <Bullet />
                            <Bullet active />
                        </Steps>
                    </Header>

                    <Title>
                        Crie sua{'\n'}conta
                    </Title>
                    <SubTitle>
                        Faça seu cadastro de{'\n'}
                        forma rápida e fácil
                    </SubTitle>

                    <Form>
                        <FormTitle>2. Senha</FormTitle>
                        <PasswordInput 
                            iconName='lock'
                            placeholder='Senha'
                            onChangeText={setPassword}
                            value={password}
                        />
                        <PasswordInput 
                            iconName='lock'
                            placeholder='Repetir senha'
                            onChangeText={setPasswordConfirm}
                            value={passwordConfirm}
                        />
                    </Form>

                    <Button 
                        title='Cadastrar'
                        color={theme.colors.sucess}
                        onPress={handleRegister}
                    />
                </KeyboardAvoidingView>
            </Container>
        </TouchableWithoutFeedback>
    );
}