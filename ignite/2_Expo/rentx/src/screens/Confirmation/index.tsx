import React from 'react';
import { StatusBar, useWindowDimensions } from 'react-native';
import { RFValue } from 'react-native-responsive-fontsize';
import { useNavigation, useRoute } from '@react-navigation/native';

import LogoSvg from '../../assets/logo_background_gray.svg';
import DoneSvg from '../../assets/done.svg';

import {
    Container,
    Content,
    Title,
    Message,
    Footer
} from './styles';
import { ConfirmButton } from '../../components/ConfirmButton';
import { AppNavigationProps } from '../../utils/stackNavigation';

interface Params {
    title: string;
    message: string;
    nextScreenRoute: any;
}

export function Confirmation(){
    const { width } = useWindowDimensions();
    const navigation = useNavigation<AppNavigationProps>();
    const route = useRoute();
    const { title, message, nextScreenRoute } = route.params as Params

    function handleHome(){
        navigation.navigate(nextScreenRoute);
    }

    return (
        <Container>
            <StatusBar 
                barStyle="light-content"
                backgroundColor="transparent"
                translucent
            />
            <LogoSvg width={width} />

            <Content>
                <DoneSvg width={RFValue(80)} height={RFValue(80)} />
                <Title>{title}</Title>

                <Message>{message}</Message>
            </Content>

            <Footer>
                <ConfirmButton title="OK" onPress={handleHome}/>
            </Footer>
        </Container>
    );
}