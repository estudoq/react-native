import React, { useState } from 'react';
import { 
    StatusBar ,
    KeyboardAvoidingView,
    TouchableWithoutFeedback,
    Keyboard,
    Alert
} from 'react-native';

import * as Yup from 'yup';

import theme from '../../styles/theme';
import { Button } from '../../components/Button';

import {
    Container,
    Header,
    Title,
    SubTitle,
    Form,
    Footer
} from './styles';
import { Input } from '../../components/Input';
import { PasswordInput } from '../../components/PasswordInput';
import { useNavigation } from '@react-navigation/native';
import { AppNavigationProps } from '../../utils/stackNavigation';

export function SignIn(){
    const navigation = useNavigation<AppNavigationProps>();

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    async function handleSignIn() {
        try {

            const schema = Yup.object().shape({
                email: Yup.string()
                    .required('E-mail obrigatório!')
                    .email('Digite um e-mail válido'),
                password: Yup.string()
                    .required('Senha é obrigatória')
            });
    
            await schema.validate({ email, password });

        } catch (error) {
            if(error instanceof Yup.ValidationError) {
                Alert.alert('ERRO', error.message);
            } else {
                Alert.alert('ERRO', 'Ocorreu um erro ao fazer login, verifique suas credenciais');
            }
        }
    }

    function handleNewAccount() {
        navigation.navigate('SignUpFirstStep');
    }

    return (
        <KeyboardAvoidingView behavior="position" enabled>
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                <Container>
                    <StatusBar 
                        barStyle="dark-content"
                        backgroundColor="transparent"
                        translucent
                    />

                    <Header>
                        <Title>Estamos {'\n'}quase lá</Title>
                        <SubTitle>
                            Faça seu login para começar {'\n'}
                            uma experiência incrível.
                        </SubTitle>
                    </Header>

                    <Form>
                        <Input 
                            iconName="mail"
                            placeholder="E-mail"
                            keyboardType="email-address"
                            autoCapitalize="none"
                            autoCorrect={false}
                            onChangeText={setEmail}
                            value={email}
                        />
                        <PasswordInput 
                            iconName="lock"
                            placeholder="Senha"
                            onChangeText={setPassword}
                            value={password}
                        />
                    </Form>

                    <Footer>
                        <Button 
                            title="Login"
                            onPress={handleSignIn}
                            enabled={false}
                            loading={false}
                        />
                        <Button 
                            title="Criar conta gratuita"
                            onPress={handleNewAccount}
                            enabled={true}
                            loading={false}
                            color={theme.colors.background_secondary}
                            light
                        />
                    </Footer>
                </Container>
            </TouchableWithoutFeedback>
        </KeyboardAvoidingView>
    );
}