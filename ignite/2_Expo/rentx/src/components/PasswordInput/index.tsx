import React, { useState } from 'react';
import { Feather } from '@expo/vector-icons';
import { useTheme } from 'styled-components';

import {
    Container,
    IconContainer,
    InputText,
    GestureHandlerView
} from './styles';

import { TextInputProps } from 'react-native';
import { BorderlessButton } from 'react-native-gesture-handler';

interface Props extends TextInputProps {
    iconName: React.ComponentProps<typeof Feather>['name'];
    value?: string;
}

export function PasswordInput({
    iconName,
    value,
    ...rest
} : Props){
    const [isPasswordVisible, setIsPasswordVisible] = useState(true);

    const [isFocused, setIsFocused] = useState(false);
    const [isFilled, setIsFilled] = useState(false);

    function handleInputFocus(){
        setIsFocused(true);
    }

    function handleInputBlur(){
        setIsFocused(false);
        setIsFilled(!!value)
    }
    
    function handlePasswordVisibilityChange() {
        setIsPasswordVisible(prevState => !prevState);
    }

    const theme = useTheme();
    
    return (
        <Container>
            <IconContainer
                isFocused={isFocused}
            >
                <Feather
                    name={iconName}
                    size={24}
                    color={(isFocused || isFilled) ? theme.colors.main : theme.colors.text_detail}
                />
            </IconContainer>

            <InputText
                isFocused={isFocused} 
                onFocus={handleInputFocus}
                onBlur={handleInputBlur}
                secureTextEntry={isPasswordVisible}
                {...rest} 
            />

            <GestureHandlerView>
                <BorderlessButton onPress={handlePasswordVisibilityChange}>
                    <Feather
                        name={isPasswordVisible ? 'eye' : 'eye-off'}
                        size={24}
                        color={theme.colors.text_detail}
                    />
                </BorderlessButton>
            </GestureHandlerView>
        </Container>
    );
}