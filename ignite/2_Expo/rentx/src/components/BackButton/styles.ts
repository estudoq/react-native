import styled from 'styled-components/native';
import { GestureHandlerRootView, BorderlessButton } from 'react-native-gesture-handler'
import { RFValue } from 'react-native-responsive-fontsize';

export const Container = styled(GestureHandlerRootView)``;

export const Button = styled(BorderlessButton)`
    width: ${RFValue(30)}px;
    height: ${RFValue(30)}px;

    align-items: center;
`;

