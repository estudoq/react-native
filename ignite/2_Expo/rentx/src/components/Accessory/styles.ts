import styled from 'styled-components/native';
import { RFValue } from 'react-native-responsive-fontsize';

export const Container = styled.View`
    flex: 1 0 26%;

    justify-content: center;
    align-items: center;
    margin-right: 5px;

    background-color: ${({ theme }) => theme.colors.background_primary};

    padding: 16px;
    margin-bottom: 8px;
`;
 
export const Name = styled.Text`
    font-family: ${({ theme }) => theme.fonts.primary_500};
    color: ${({ theme }) => theme.colors.text};
    font-size: ${RFValue(13)}px;
`;