import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import { SignIn } from '../screens/SignIn';
import { PublicRoutesParamList } from '../@types/navigation';

const { Navigator, Screen } = createStackNavigator<PublicRoutesParamList>();

export function AuthRoutes(){
    return (
        <Navigator
            screenOptions={{
                headerShown: false
            }}
        >
            <Screen 
                name="SignIn"
                component={SignIn}
            />
        </Navigator>
    );
}