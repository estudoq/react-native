import React from 'react';
import { categories } from '../../utils/categories';

import { 
    Container,
    Header,
    Title,
    DeleteIcon,
    Amount,
    Footer,
    Category,
    Icon,
    CategoryName,
    Date, 
    GestureHandlerView,
    DeleteButton
} from './styles'

export interface TransactionCardProps {
    type: 'positive' | 'negative';
    name: string;
    amount: string;
    category: string;
    date: string;
}

interface Props {
    data: TransactionCardProps;
    onPress: () => void;
}

export function TransactionCard({ 
    data,
    onPress
} : Props){
    const [ category ] = categories.filter(item => item.key === data.category);

    return (
        <Container>
            <Header>
                <Title>
                    {data.name}
                </Title>

                <GestureHandlerView>
                    <DeleteButton onPress={onPress}>
                        <DeleteIcon name="trash" />
                    </DeleteButton>
                </GestureHandlerView>
            </Header>

            <Amount type={data.type}>
                {data.type === 'negative' && '- '}
                {data.amount}
            </Amount>

            <Footer>
                <Category>
                    <Icon name={category.icon} />
                    <CategoryName>
                        {category.name}
                    </CategoryName>
                </Category>

                <Date>
                    {data.date}
                </Date>
            </Footer>
        </Container>
    );
}