import React from 'react';

import { 
    Container,
    DateContainer,
    DatePickerButton,
    DatePickerIcon,
    DateSelected,
    Picker
 } from './styles';

interface Props {
    date: Date;
    onDateChange: (newDate: Date | undefined) => void;
    onPress: () => void;
    showDatePicker: boolean;
}

export function DateSelect({
    date,
    onDateChange,
    onPress,
    showDatePicker
} : Props){

    return (
        <Container>
            <DateContainer>
                <DateSelected>
                    {date.toLocaleString('pt-BR', { day:'numeric', month: 'numeric', year:'numeric' })}
                </DateSelected>
            </DateContainer>

            <DatePickerButton onPress={onPress}>
                <DatePickerIcon name="calendar" />
            </DatePickerButton>

            {showDatePicker && (
                <Picker
                    value={date}
                    mode="date" 
                    display="calendar"
                    onChange={(_event, newDate) => onDateChange(newDate)}
                />
            )}
            
        </Container>
    );
}