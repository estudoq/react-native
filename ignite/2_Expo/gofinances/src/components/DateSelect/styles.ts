import styled from 'styled-components/native';
import DateTimePicker from '@react-native-community/datetimepicker';
import { Feather } from '@expo/vector-icons';
import { RFValue } from 'react-native-responsive-fontsize';

export const Container = styled.View`
    width: 100%;

    align-items: center;
    flex-direction: row;

    margin-top: 15px;
`;

export const DateContainer = styled.View`
    width: 90%;
    padding: ${RFValue(16)}px;

    background-color: ${({ theme }) => theme.colors.shape};
    border-radius: 5px;
    
    align-items: center;
    justify-content: center;
    margin-right: 5px;
`;

export const DateSelected = styled.Text`
    font-size: ${RFValue(14)}px;
    font-family: ${({ theme }) => theme.fonts.regular};
`;

export const Picker = styled(DateTimePicker)`
    width: 100%;
    color: ${({ theme }) => theme.colors.secondary};
`;

export const DatePickerButton = styled.TouchableOpacity``;

export const DatePickerIcon = styled(Feather)`
    font-size: ${RFValue(24)}px;
    font-family: ${({ theme }) => theme.fonts.bold};
    color: ${({ theme }) => theme.colors.secondary};
`;