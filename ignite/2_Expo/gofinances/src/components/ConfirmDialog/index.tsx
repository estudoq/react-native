import Dialog from 'react-native-dialog';

import { Container } from './styles';

export interface ConfirmDialogProps {
    title: string;
    content: string;
    labelNegativeButton: string;
    labelPositiveButton: string;
    negativeOnPress: () => void;
    positiveOnPress: () => void;
    visibility: boolean;
}

export function ConfirmDialog({
    title,
    content,
    labelNegativeButton,
    labelPositiveButton,
    negativeOnPress,
    positiveOnPress,
    visibility
} : ConfirmDialogProps) {
    return (
        <Container>
            <Dialog.Container visible={visibility}>
                <Dialog.Title>
                    {title}
                </Dialog.Title>
                <Dialog.Description>
                    {content}
                </Dialog.Description>
            <Dialog.Button label={labelNegativeButton} onPress={negativeOnPress} />
            <Dialog.Button label={labelPositiveButton} onPress={positiveOnPress}/>
            </Dialog.Container>
        </Container>
    );
}