import React, { useCallback, useState } from 'react';
import { 
    Modal, 
    TouchableWithoutFeedback, 
    Keyboard,
    Alert 
} from 'react-native';
import * as Yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import AsyncStorage from '@react-native-async-storage/async-storage';
import uuid from 'react-native-uuid';
import { useFocusEffect, useNavigation } from '@react-navigation/native';

import { useForm } from 'react-hook-form';

import { InputForm } from '../../components/Form/InputForm';
import { Button } from '../../components/Form/Button';
import { TransactionTypeButton } from '../../components/Form/TransactionTypeButton';
import { CategorySelectButton } from '../../components/Form/CategorySelectButton';
import { CategorySelect } from '../CategorySelect';

import { 
    Container,
    Header,
    Title,
    Form,
    Fields,
    TransactionsTypes,
    ScrollFragment
 } from './styles';
import { AppNavigationProps } from '../../@types/navigation';
import { DateSelect } from '../../components/DateSelect';
import { useAuth } from '../../hooks/auth';

interface FormData {
    name: string;
    amount: string;
}

const schema = Yup.object().shape({
    name: Yup
    .string()
    .required('Nome é obrigatório!'),
    amount: Yup
    .number()
    .typeError('Informe um valor numérico!')
    .positive('O valor não pode ser negativo!')
    .required('O valor é obrigatório!')
});

export function Register() {
    const [transactionType, setTransactionType] = useState('');
    const [categoryModalOpen, setCategoryModalOpen] = useState(false);

    const { user } = useAuth();

    const [showDatePicker, setShowDatePicker] = useState(false);
    const [date, setDate] = useState(new Date());

    const [category, setCategory] = useState({
        key: 'category',
        name: 'Categoria'
    });

    const {
        control,
        handleSubmit,
        reset,
        formState: { errors }
    } = useForm({
        resolver: yupResolver(schema)
    });

    const navigation = useNavigation<AppNavigationProps>();

    function handleShowDatePicker(){
        setShowDatePicker(true);
    }

    function handleChangeDate(selectedDate: Date | undefined){
        setShowDatePicker(false);

        if (selectedDate !== undefined) {
            const currentDate = new Date();

            selectedDate!.setHours(0, 0, 0, 0);
            currentDate!.setHours(0, 0, 0, 0);

            if (selectedDate! > currentDate) {
                Alert.alert("ATENÇÃO", "Não é possível escolher uma data no futuro!");
                return;
            }

        } else {
            selectedDate = new Date();
        }

        setDate(selectedDate!);
    }

    function handleTransactionsTypeSelect(type: 'positive' | 'negative') {
        setTransactionType(type);
    }

    function handleOpenSelectCategoryModal(){
        setCategoryModalOpen(true);
    }

    function handleCloseSelectCategoryModal(){
        setCategoryModalOpen(false);
    }

    async function handleRegister(form: FormData){
        if(!transactionType) 
            return Alert.alert('ATENÇÃO', 'Selecione o tipo da transação!');

        if(category.key === 'category') 
            return Alert.alert('ATENÇÃO', 'Selecione a categoria da transação!');

        const newTransaction = {
            id: String(uuid.v4()),
            name: form.name,
            amount: form.amount,
            type: transactionType,
            category: category.key,
            date
        }

        try {
            const dataKey = `@gofinances:transactions_user:${user.id}`;

            const data = await AsyncStorage.getItem(dataKey);
            const currentData = data ? JSON.parse(data!) : [];

            const dataFormatted = [
                ...currentData,
                newTransaction
            ]

            await AsyncStorage.setItem(dataKey, JSON.stringify(dataFormatted));

            setTransactionType('');
            setCategory({
                key: 'category',
                name: 'Categoria'
            })
            reset();
            navigation.navigate("Listagem");

        } catch (error) {
            console.log(error);
            Alert.alert("ERRO", "Não foi possível salvar a transação: \n" + error);
        }
    }

    useFocusEffect(useCallback(() => {
        setDate(new Date());
    }, []));

    return (
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <Container>
                <Header>
                    <Title>
                        Cadastro
                    </Title>
                </Header>

                <Form>
                    <ScrollFragment>
                        <Fields>
                            <InputForm
                                name="name"
                                control={control}
                                placeholder="Nome"
                                autoCapitalize="sentences"
                                autoCorrect={false}
                                error={errors.name && errors.name.message}
                            />

                            <InputForm
                                name="amount"
                                control={control} 
                                placeholder="Preço"
                                keyboardType="numeric"
                                error={errors.amount && errors.amount.message}
                            />

                            <TransactionsTypes>
                                <TransactionTypeButton 
                                    type="up"
                                    title="Income"
                                    onPress={() => handleTransactionsTypeSelect('positive')}
                                    isActive={transactionType === 'positive'}
                                />
                                <TransactionTypeButton 
                                    type="down"
                                    title="Outcome"
                                    onPress={() => handleTransactionsTypeSelect('negative')}
                                    isActive={transactionType === 'negative'}
                                />
                            </TransactionsTypes>

                            <CategorySelectButton 
                                title={category.name}
                                onPress={handleOpenSelectCategoryModal}
                            />

                            <DateSelect 
                                date={date}
                                onDateChange={handleChangeDate}
                                onPress={handleShowDatePicker}
                                showDatePicker={showDatePicker}
                            />
                        </Fields>
                    </ScrollFragment>

                    <Button 
                        title="Enviar"
                        onPress={handleSubmit(handleRegister)}
                    />
                </Form>

                <Modal visible={categoryModalOpen}>
                    <CategorySelect 
                        category={category}
                        setCategory={setCategory}
                        closeSelectCategory={handleCloseSelectCategoryModal}
                    />
                </Modal>
            </Container>
        </TouchableWithoutFeedback>
    );
}