import React, { useCallback, useEffect, useState } from 'react';
import { ActivityIndicator, Alert } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

import { useFocusEffect } from '@react-navigation/native';
import { useTheme } from 'styled-components';

import { HighlightCard } from '../../components/HighlightCard';
import { TransactionCard, TransactionCardProps } from '../../components/TransactionCard';

import { 
    Container,
    Header,
    UserWrapper, 
    UserInfo,
    Photo,
    User,
    UserGreeting,
    UserName,
    Icon,
    HighlightCards,
    Transactions,
    Title,
    TransactionList,
    LogoutButton,
    GestureHandlerView,
    LoadContainer
} from './styles'
import { ConfirmDialog } from '../../components/ConfirmDialog';
import { useAuth } from '../../hooks/auth';

export interface DataListProps extends TransactionCardProps {
    id: string;
}

interface HighlightProps {
    amount: string;
    lastTransaction: string;
}

interface HighlightData {
    entries: HighlightProps;
    expensives: HighlightProps;
    total: HighlightProps;
}

export function DashBoard(){
    const [isLoading, setIsLoading] = useState(true);
    const [transactions, setTransactions] = useState<DataListProps[]>([]);
    const [highlightData, setHighlightData] = useState<HighlightData>({} as HighlightData);

    const [dialogVisibility, setDialogVisibility] = useState(false);
    const [selectedTransactionId, setSelectedTransactionId] = useState('');
    const [dialogContent, setDialogContent] = useState('');

    const theme = useTheme();
    const { signOut, user } = useAuth();

    const dataKey = `@gofinances:transactions_user:${user.id}`;

    function getLastTransactionDate(
        collection: DataListProps[],
        type: 'positive' | 'negative'
    ) {
        const lastTransaction = new Date(
            Math.max.apply(Math, collection
                .filter(transaction => transaction.type === type)
                .map(transaction => new Date(transaction.date).getTime()))
        );

        if(lastTransaction.getDate().toString() === 'NaN') return null;

        return `${lastTransaction.getDate()} de ${lastTransaction.toLocaleString(`pt-BR`, { month: 'long' })}`;
    }

    function showDialog(transactionName: string, transactionId: string) {
        setSelectedTransactionId(transactionId);
        setDialogContent("Deseja realmente excluir a transação \"" + transactionName + "\"?");
        setDialogVisibility(true);
    };

    function handleCancelDialog(){
        setDialogVisibility(false);
    };

    async function handleRemoveTransaction(id: string){
        try {
            const response = await AsyncStorage.getItem(dataKey);
            const storagedTransactions = response ? JSON.parse(response) : [];

            const filteredTransactions = storagedTransactions.filter(
                (transaction: DataListProps) => transaction.id !== id
            );
        
            setTransactions(filteredTransactions);

            await AsyncStorage.setItem(dataKey, JSON.stringify(filteredTransactions));

            handleCancelDialog();
            loadTransactions();
        } catch {
            Alert.alert("Não foi possível concluir a operação!");
        }
    }

    async function loadTransactions(){
        const response = await AsyncStorage.getItem(dataKey);

        const transactions = response ? JSON.parse(response) : [];

        let entriesTotal = 0;
        let expensiveTotal = 0;

        const transactionsFormatted: DataListProps[] = transactions.map((item: DataListProps) => {

            if(item.type === 'positive') {
                entriesTotal += Number(item.amount);
            } else {
                expensiveTotal += Number(item.amount);
            }

            const amount = Number(item.amount).toLocaleString('pt-BR', {
                style: 'currency',
                currency: 'BRL'
            });

            const date = Intl.DateTimeFormat('pr-BR', {
                day: '2-digit',
                month: '2-digit',
                year: '2-digit'
            }).format(new Date(item.date));

            return {
                id: item.id,
                name: item.name,
                amount,
                type: item.type,
                category: item.category,
                date
            }
        });

        setTransactions(transactionsFormatted);

        const lastTransactionEntries = getLastTransactionDate(transactions, "positive");
        const lastTransactionExpensives = getLastTransactionDate(transactions, "negative");
        let totalInterval = '';

        if (lastTransactionExpensives == null) {
            const currentDate = new Date();

            totalInterval = `01 a ${currentDate.getDate()} de ${currentDate.toLocaleString(`pt-BR`, { month: 'long' })}`
        } else {
            totalInterval = `01 a ${lastTransactionExpensives}`;
        }

        const total = entriesTotal - expensiveTotal;

        setHighlightData({
            entries: {
                amount: entriesTotal.toLocaleString('pt-BR', {
                    style: 'currency',
                    currency: 'BRL'
                }),
                lastTransaction: lastTransactionEntries !== null ? `Última entrada dia ${lastTransactionEntries}` : 'Não há transações de entrada'
            },
            expensives: {
                amount: expensiveTotal.toLocaleString('pt-BR', {
                    style: 'currency',
                    currency: 'BRL'
                }),
                lastTransaction: lastTransactionExpensives !== null ? `Última saída dia ${lastTransactionExpensives}` : 'Não há transações de saída'
            },
            total: {
                amount: total.toLocaleString('pt-BR', {
                    style: 'currency',
                    currency: 'BRL'
                }),
                lastTransaction: totalInterval
            }
        });

        setIsLoading(false);
    }

    useEffect(() => {
        loadTransactions();
    }, []);

    useFocusEffect(useCallback(() => {
        loadTransactions();
    }, []));

    return (
        <Container>
            {
                isLoading ? 
                <LoadContainer>
                    <ActivityIndicator 
                        color={theme.colors.primary}
                        size="large"
                    />
                </LoadContainer> : 
                <>
                    <Header>
                        <UserWrapper>
                            <UserInfo>
                                <Photo 
                                    source={{ uri: user.photo }}
                                />
                                <User>
                                    <UserGreeting>Olá, </UserGreeting>
                                    <UserName>{ user.name }</UserName>
                                </User>
                            </UserInfo>

                            <GestureHandlerView>
                                <LogoutButton onPress={signOut}>
                                    <Icon name='power' />
                                </LogoutButton>
                            </GestureHandlerView>
                        </UserWrapper>
                    </Header>

                    <HighlightCards>
                        <HighlightCard 
                            type='up'
                            title='Entradas' 
                            amount={highlightData.entries.amount}
                            lastTransaction={highlightData.entries.lastTransaction}
                        />
                        <HighlightCard 
                            type='down'
                            title='Saídas' 
                            amount={highlightData.expensives.amount}
                            lastTransaction={highlightData.expensives.lastTransaction}
                        />
                        <HighlightCard 
                            type='total'
                            title='Total' 
                            amount={highlightData.total.amount}
                            lastTransaction={highlightData.total.lastTransaction}
                        />
                    </HighlightCards>

                    <Transactions>
                        <Title>
                            Listagem
                        </Title>

                        <TransactionList
                            data={transactions}
                            keyExtractor={item  => item.id}
                            renderItem={({ item }) => 
                                <TransactionCard data={item} onPress={() => showDialog(item.name, item.id)}/> 
                            }
                        />

                        <ConfirmDialog 
                            title="Excluir transação"
                            content={dialogContent}
                            labelNegativeButton="Cancelar"
                            labelPositiveButton="Excluir"
                            negativeOnPress={handleCancelDialog}
                            positiveOnPress={() => handleRemoveTransaction(selectedTransactionId)}
                            visibility={dialogVisibility}
                        />
                    </Transactions>
                </>
            }
        </Container>
    );
}
