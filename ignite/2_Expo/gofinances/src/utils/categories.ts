export const categories = [
    { key: 'purchases', name: 'Compras', icon: 'shopping-bag', color: '#5636D3' },
    { key: 'food', name: 'Alimentação', icon: 'coffee', color: '#FF872C' },
    { key: 'salary', name: 'Salário', icon: 'dollar-sign', color: '#12A454' },
    { key: 'car', name: 'Carro', icon: 'crosshair', color: '#E83F5B' },
    { key: 'leisure', name: 'Lazer', icon: 'smile', color: '#26195C' },
    { key: 'studies', name: 'Estudos', icon: 'book', color: '#9C001A' },
    { key: 'bills', name: 'Contas', icon: 'percent', color: '#03256C' },
    { key: 'health', name: 'Saúde', icon: 'heart', color: '#048A81' },
]; 