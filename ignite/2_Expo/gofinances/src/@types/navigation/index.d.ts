import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs";

export type PublicRoutesParamList = {
    SignIn: undefined;
}

export type PrivateRoutesParamList = {
    Listagem: undefined;
    Cadastrar: undefined;
    Resumo: undefined;
};

export type AppNavigationProps = BottomTabNavigationProp<PrivateRoutesParamList>;