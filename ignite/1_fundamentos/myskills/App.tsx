import React from 'react';
import { StatusBar } from 'react-native';
import { Home } from './src/pages/Home';

export default function App(){
  return (
    //A tag <>  é uma alias para 'Fragment', um agrupador que não possui efeito visual
    <>
      <StatusBar barStyle='light-content' />
      <Home />
    </>
  )
}